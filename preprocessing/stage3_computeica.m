function m03_preprp_runICA(selsubj)
%clear all;

% eeglab
%[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
cfg = get_cfg([]);
addpath('functions');
addpath(cfg.dir_eeglab)
eeglab rebuild;
addpath(cfg.dir_jsonlab)

curdir = pwd();

who_idx = dir([cfg.dir_bids '/derivatives/eegprep/sub*']);
if nargin == 0
    selsubj = 1:length(who_idx);
end
curdir = pwd();
for isub = selsubj

    cd(curdir);
    
    EEG = [];
    % get cfgs
    cfg = get_cfg(who_idx(isub).name);
    sess = 'ses-01';
    % Load data set.
    EEG = pop_loadset( 'filename',[cfg.subject_name  '_' sess '_stage2.set'] ,'filepath',cfg.dir_eeg);
    EEG = pop_select(EEG,'nochannel',{'AUDIO','ECG','HEOG','VEOG','MISC','M1','M2'});
    
    if cfg.hp_ICA_filter
        % apply high-pass filter (e.g. 1Hz)
        cfg2 = cfg;
        cfg2.hp_filter_limit = cfg.hp_ICA_filter_limit;
        cfg2.hp_filter_tbandwidth = 1;
        EEG2 = preproc_hpfilter(EEG,cfg2);
    else
        EEG2 = EEG;
    end
    
    % go to directory
    cd(cfg.dir_eeg)
    
    if cfg.binica
        mkdir('binica_output')
        fprintf('Let EEGLAB calculate the number of components to extract.\n')
        % remove threshold estimation trials (we focus on 'adapt' session)
        EEG3 = pop_selectevent(EEG2,'type',cfg.trig_target);
        % put trials together
        EEG3.data = reshape(EEG3.data,size(EEG3.data,1),size(EEG3.data,2)*size(EEG3.data,3));
        % apply PCA
        [COEFF,SCORE,latent,tsquare] = pca(EEG3.data');
        % keep components corresponding to 99% of variance
        EEG3.num_components_to_keep = find(cumsum(latent) ./ sum(latent)>0.99,1)+1;   %change the value from 0.98-0.99-0.995-0.998-1, if sometimes the ICA is not done
        % run BINICA
        EEG3 = pop_runica(EEG3, 'icatype', 'binica' ,'pca', EEG3.num_components_to_keep, 'chanind', 1:length(EEG3.chanlocs));%, 'pca', length(EEG.chanlocs));,'pca', 8,
        
    end
    
    %%
    
    %copy weight & sphere to original data
    EEG.icaweights  = EEG3.icaweights;
    EEG.icasphere   = EEG3.icasphere;
    EEG.icawinv     = EEG3.icawinv;
    EEG.icachansind = EEG3.icachansind;
    EEG.icaact      = EEG3.icaact;
    EEG = eeg_checkset(EEG);
        
    if cfg.hp_ICA_filter
        EEG2.icaweights  = EEG3.icaweights;
        EEG2.icasphere   = EEG3.icasphere;
        EEG2.icawinv     = EEG3.icawinv;
        EEG2.icachansind = EEG3.icachansind;
        EEG2.icaact      = EEG3.icaact;
        EEG2 = eeg_checkset(EEG2);
%        eeglab redraw
    end

    % ----------
    % Save data.
    % ----------
    EEG = pop_editset(EEG, 'setname', [cfg.subject_name '_ses-01_stage3.set']);
    EEG = pop_saveset(EEG, [cfg.subject_name '_ses-01_stage3.set'],cfg.dir_eeg);
    
    EEG2 = pop_editset(EEG2, 'setname', [cfg.subject_name '_ses-01_stage3_1hz.set']);
    EEG2 = pop_saveset(EEG2, [cfg.subject_name '_ses-01_stage3_1hz.set'],cfg.dir_eeg);
end
fprintf('Done.\n')

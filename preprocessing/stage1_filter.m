function m01_prepro(selsubj)

 cfg = get_cfg([]);
addpath('functions');
addpath(cfg.dir_eeglab)
eeglab rebuild;
addpath(cfg.dir_jsonlab)

maxNumCompThreads(10)

who_idx = dir([cfg.dir_bids '/sub*']);
if nargin == 0
    selsubj = 1:length(who_idx);
end

for isub = selsubj;%[2:6,8:12,15:18],%length(who_idx)
    % get cfgs
    cfg = get_cfg(who_idx(isub).name);
    EEG = [];
    
     %% IMPORT EEG AND EVT DATA (BIDS FORMAT)
    fprintf(' [ stage 1 ] reading data from subject %s, (number %d of %d to process).\n',who_idx(isub).name,isub,length(selsubj));

    eegfiles = dir([cfg.dir_raw '*.set']);    
    eegData = [eegfiles(1).folder filesep eegfiles(1).name];              % for all subs with 3 cnt files
    
    if ~exist(eegData,'file')
        error('%s Does not exist!\n',eegData)
    else
        EEG = pop_loadset(eegData);
        origfs = EEG.srate;
        
        [subj,sess,task,run] = parse_bidsfile(EEG.filename);
        chans = loadevents([EEG.filepath '/' subj '_' sess '_channels.tsv']);
        evtData = [eegData(1:end-8) '_events.tsv'];
        evts = loadevents(evtData);
        %evts.event_sample = evts.event_sample*ratio;
       
        for i=2:length(eegfiles)  
            maxsamp = EEG.pnts;
            eegData2 = [eegfiles(i).folder filesep eegfiles(i).name];              % for all subs with 3 cnt files
            EEG2 = pop_loadset(eegData2);
            ratio = 1;
            if EEG2.srate ~= EEG.srate
                cfg2 = cfg;
                cfg2.new_sampling_rate = EEG.srate
                [ EEG2,ratio] = preproc_resample( EEG2,cfg2 )
            end
            
            EEG = pop_mergeset(EEG, EEG2);    
            evtData = [eegData2(1:end-8) '_events.tsv'];
            evts_ = loadevents(evtData);
            evts_.onset = evts_.onset + maxsamp./EEG.srate;
            evts_.sample = evts_.sample*ratio + maxsamp;
            
            evts = [evts;evts_];
        end
%       EEG = eeg_eegrej(EEG, [1 1146880]);  %  for pilot 2 to remove the down and up data
    end
%    

    
    EEG_org = EEG;
    
    %% Copy events from _events.tsv file
    % a part from latency duration and type, copies events corresponding to
    % strings in cfg.events
    fprintf(' [ stage 1 ] copying events\n');
    
    event = [];
    for e=1:size(evts,1)
        event(e).latency = evts.sample(e);
        event(e).duration = evts.duration(e);
        event(e).type = evts.trial_type{e};
        
        for field = cfg.events
            val = evts.(cell2mat(field))(e);
            if isstring(val), val = str2double(val); end
            event(e).(cell2mat(field)) = val;
        end
    end
    oldevents = EEG.event;
    EEG.event = event;
    EEG = eeg_checkset(EEG, 'eventconsistency');    
    
    
   
%%
    fprintf(' [ stage 1 ] resampling and lowpass filtering\n');
    [EEG,ratio] = preproc_resample(EEG,cfg);
    EEG = preproc_hpfilter(EEG,cfg);
    EEG = preproc_lpfilter(EEG,cfg);
   
    %% Rename channels
    fprintf(' [ stage 1 ] checking channels\n');

    [EEG] = pop_chanedit(EEG,'lookup','elecs.elp');
   
 
    EEG = pop_select(EEG,'nochannel',{'EventCh0','EventCh1'});

    %% Save data
    fprintf(' [ stage 1 ] saving data\n');
    [EEG, com] = pop_editset(EEG, 'setname', [cfg.subject_name '_' sess '_stage1']);
    EEG = eegh(com, EEG);
    if ~exist(cfg.dir_eeg,'dir')
        system(sprintf('mkdir -p %s',cfg.dir_eeg))
    end
    pop_saveset( EEG, 'filename',[cfg.subject_name  '_' sess '_stage1.set'] ,'filepath',cfg.dir_eeg);
    % eeglab redraw
end
fprintf('Done.\n')

function stage6_cleanup(selsubj)

   cfg = get_cfg([]);
addpath('functions');
addpath(cfg.dir_eeglab)
eeglab rebuild;
addpath(cfg.dir_jsonlab)


who_idx = dir([cfg.dir_bids '/derivatives/eegprep/sub*']);
if nargin == 0
    selsubj = 1:length(who_idx);
end
for isub = selsubj

    
    EEG = [];
    
    % get cfgs
    cfg = get_cfg(who_idx(isub).name);
    
    %%
    evtData1 = [cfg.dir_raw '/' cfg.subject_name '_ses-01_task-agency_run-01_events.tsv'];
    evts1 = loadevents(evtData1);
    evts1 = evts1(strcmp(evts1.trial_type,'stim-adapt'),:);
    
    evtData2 = [cfg.dir_raw '/' cfg.subject_name '_ses-01_task-agency_run-02_events.tsv'];
    evts2 = loadevents(evtData2);
    evts2 = evts2(strcmp(evts2.trial_type,'stim-thr'),:);
    
    evtData3 = [cfg.dir_raw '/' cfg.subject_name '_ses-01_task-agency_run-03_events.tsv'];
    evts3 = loadevents(evtData3);
    evts3 = evts3(strcmp(evts3.trial_type,'stim-thr'),:);
    
    evts = [evts1 ; evts2 ; evts3];
    evts = [table((1:size(evts,1)).','VariableNames', {'trialid'}) evts ]
    rmtrials1 = csvread([cfg.dir_eeg '/' cfg.subject_name '_ses-01_stage2_rejtrials.csv']);
    evts(rmtrials1,:) = [];
    file = [cfg.dir_eeg '/' cfg.subject_name '_ses-01_stage5_rejtrials.csv'];
    if exist(file,'file')
        rmtrials2 = csvread(file);
        evts(rmtrials2,:) = [];
    else
        rmtrials2 = [];
    end
    %%
    % Load data set.
     EEG_1hz = pop_loadset('filename',[cfg.subject_name '_ses-01_stage4_1hz.set'],...
        'filepath',cfg.dir_eeg,'loadmode','all');
    if ~isempty(rmtrials2)
        [EEG_1hz, com] = pop_rejepoch(EEG_1hz, rmtrials2, 1);
    end
    % Fix the fact that EOG and ECG were not high-pass filtered
%     cfg2 = cfg;
%     cfg2.hp_filter_limit = cfg.hp_ICA_filter_limit;
%     cfg2.hp_filter_tbandwidth = 1;
%     EEG2 = preproc_hpfilter(EEG_1hz,cfg2); close; pause(0.1);
%     idveog = strcmp({EEG2.chanlocs.labels},'VEOG');
%     idheog = strcmp({EEG2.chanlocs.labels},'HEOG');
%     idecg = strcmp({EEG2.chanlocs.labels},'ECG');
%     veog  = EEG2.data(idveog,:,:);
%     heog  = EEG2.data(idheog,:,:);
%     ecg  = EEG2.data(idecg,:,:);
%     [EEG_1hz,com]  = pop_select(EEG_1hz,'nochannel',{'VEOG' 'HEOG','ECG'});
    EEG_1hz = pop_interp(EEG_1hz, EEG_1hz.full_chanlocs, 'spherical');
    
%     % replace ECG/EOG
%     EEG_1hz.data(end+1,:,:)  = veog;  
%   	EEG_1hz.data(end+1,:,:)  = heog;  
%     EEG_1hz.data(end+1,:,:)  = ecg;  
%     EEG_1hz.chanlocs(end+1).labels = 'VEOG';
%     EEG_1hz.chanlocs(end+1).labels = 'HEOG';
%     EEG_1hz.chanlocs(end+1).labels = 'ECG';
%    
%     EEG_1hz = eeg_checkset(EEG_1hz);
%     
     %%
    % remove r-peak triggers that were badly processed
    mark = false(1,length(EEG_1hz.event));
    for e=1:length(EEG_1hz.event)
        if strcmp(EEG_1hz.event(e).type,'r-peak')
            mark(e) = 1;
        end
    end
    EEG_1hz.event(mark) = [];
    
    %%
    catches = strcmp({EEG_1hz.epoch.eventsdt},'crej') | strcmp({EEG_1hz.epoch.eventsdt},'fa');
    thrs = strcmp({EEG_1hz.epoch.eventsdt},'n/a');
    
    for e=1:length(EEG_1hz.epoch)
        EEG_1hz.newepoch(e).id = evts.trialid(e);
        if catches(e)
           EEG_1hz.newepoch(e).stimamp = 0;
        end
        if thrs(e)
           EEG_1hz.newepoch(e).type = 'thr';
        else
           EEG_1hz.newepoch(e).type = 'adapt';
        end
        EEG_1hz.newepoch(e).conf = EEG_1hz.epoch(e).eventconfidence{1};
        EEG_1hz.newepoch(e).sdt = cell2mat(EEG_1hz.epoch(e).eventsdt{1});
        EEG_1hz.newepoch(e).stimamp = EEG_1hz.epoch(e).eventstimamp{1};
        EEG_1hz.newepoch(e).stimonset = EEG_1hz.epoch(e).eventstimon{1};
        EEG_1hz.newepoch(e).rt = EEG_1hz.epoch(e).eventresponse_time{1};
        EEG_1hz.newepoch(e).rt_conf = EEG_1hz.epoch(e).eventresponse_time2{1};
        
        [pks, ipks] = findpeaks(zscore(-EEG2.data(idecg,:,e)),'MinPeakHeight',3,'MinPeakDistance',100);
        ifirst = find(EEG_1hz.times(ipks)*1e-3 > 0,1,'first');
        first = EEG_1hz.times(ipks(ifirst))*1e-3;
        ilast = find(EEG_1hz.times(ipks)*1e-3 <= 0,1,'last');
        last = -EEG_1hz.times(ipks(ilast))*1e-3;
        EEG_1hz.newepoch(e).last_rpeak = last;
        EEG_1hz.newepoch(e).first_rpeak = first; 
    end
    EEG_1hz.epoch = EEG_1hz.newepoch;
    EEG_1hz = rmfield(EEG_1hz,'newepoch');
    EEG_1hz = eeg_checkset(EEG_1hz);
    EEG_1hz = pop_editset(EEG_1hz, 'setname', [cfg.subject_name '_preproc_1hz.set']);
    EEG_1hz = pop_saveset(EEG_1hz,[cfg.subject_name '_preproc_1hz.set'] ,cfg.dir_eeg);
    
    %%
    
    % load previous 0.1 hz dataset
    EEG = pop_loadset('filename',[cfg.subject_name '_ses-01_stage3.set'],...
        'filepath',cfg.dir_eeg,'loadmode','all');
           
    % ICA
    [EEG,com] = pop_subcomp(EEG,EEG_1hz.rm_ICs,1);
    if ~isempty(rmtrials2)
        [EEG, com] = pop_rejepoch(EEG, rmtrials2, 1);
    end
    % Reinterpolate
    [EEG,com]  = pop_select(EEG,'nochannel',{'VEOG' 'HEOG','ECG'});
    EEG = pop_interp(EEG, EEG.full_chanlocs, 'spherical');
    
    % replace ECG/EOG
    EEG.data(end+1,:,:)  = veog;  
  	EEG.data(end+1,:,:)  = heog;  
    EEG.data(end+1,:,:)  = ecg;  
    EEG.chanlocs(end+1).labels = 'VEOG';
    EEG.chanlocs(end+1).labels = 'HEOG';
    EEG.chanlocs(end+1).labels = 'ECG';
    
    if cfg.do_reref_after_ica
        [EEG, com] = pop_reref(EEG,[],'keepref','on');
        EEG = eegh(com, EEG);
    end
    EEG.epoch = EEG_1hz.epoch;
    EEG.event = EEG_1hz.event;
    EEG = eeg_checkset(EEG);
    
    EEG = pop_editset(EEG, 'setname', [cfg.subject_name '_preproc_01hz.set']);
    EEG = pop_saveset(EEG,[cfg.subject_name '_preproc_01hz.set'] ,cfg.dir_eeg);
   
   fprintf('\n == subj %s: %d/%d channels\n\n',cfg.subject_name,size(EEG.data,1),size(EEG_1hz.data,1));
    figure(601); clf;
    subplot(121); 
    findpeaks(zscore(-EEG2.data(idecg,:,e)),'MinPeakHeight',3,'MinPeakDistance',100);
    title(cfg.subject_name);
    subplot(122);
    plot(mean(EEG.data(16,:,:),3))
    hold on;
    plot(mean(EEG_1hz.data(16,:,:),3))
    pause(0.1);
end

fprintf('Done.\n')
function [ EEG,ratio] = preproc_resample( EEG,cfg )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
origfs = EEG.srate;
if cfg.do_resampling
    % --------------------------------------------------------------
    % Downsample data. Removing and adding back the path is necessary for
    % avoiding an error of the resample function. Not sure why. Solution is
    % explained here: https://sccn.ucsd.edu/bugzilla/show_bug.cgi?id=1184
    % --------------------------------------------------------------

    [pathstr, ~, ~] = fileparts(which('resample.m'));
    rmpath([pathstr '/'])
    addpath([pathstr '/'])
    [EEG, com] = pop_resample( EEG, cfg.new_sampling_rate);
    EEG = eegh(com, EEG);
end
ratio = (EEG.srate/ origfs);

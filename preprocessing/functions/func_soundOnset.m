function EEG = func_soundOnset(EEG,cfg)
% be careful the threshold for each subject maybe different, need to check the sound signal to decide
% 1. sound_peak_th  
% 2. stimonset_diff

% EEG_org =EEG;
%% check the peak of sound stim
%
sound_peak_th = 0.2E4;            % 0.2E4 for ususl,0.15E4 for suj2, 1.3E4 for suj8,

stim      =  find(ismember({EEG.event.type},'100'));      % position of vibration event
stimonset =  [EEG.event(stim).latency];                   % data points of the vibrator event

EEG_sound =[];  peak = [];  position=[];
EEG_sound =  EEG.data(65,:) - mean(EEG.data(65,:));       %baseline corr for sound
[peak, position] = findpeaks(EEG_sound,'MinPeakHeight',sound_peak_th);

% stimonset_diff=[]; 
% num = 1;
% for i = 2:length(stimonset)
%    stimonset_diff (num) =  stimonset(i) -stimonset(i-1);
%    num = num +1;
% end


sound_start = [];   % sample points for the start of each sound stim
sound_peak =[];
j = 2;
for i = 1:length(position)
    if i == 1
        sound_start(1) = position (i);  
        sound_peak(1)  = peak(i);
    elseif (position(i) - position(i-1)) > 100  % min(stimonset_diff)  % 80 is depond on subject
        sound_start(j) = position(i);
        sound_peak(j)  = peak(i);
        j = j + 1;
    end
end
% sound_start([361 362]) =[];        %% 361-362 is noise for subject 11
% sound_start([401 402]) =[];        %% 401-402 is noise for subject 13
length(sound_start)   


% 
%check form the figure
% figure(2)
% plot(EEG_sound)  %sound siganl 
% hold on
% plot(stimonset,0.2E4,'r*')  % trigger mark   0.2E4,
% hold on
% plot(sound_start,sound_peak,'g*')    %peaks of sound, the first peak of the sound points

%% add sound onset in EEG.event

event_num = length(EEG.event);
for i = 1:length(sound_start)
    EEG.event(event_num + i).latency  = sound_start(i);
    EEG.event(event_num + i).type     = '20';  % marker 20 for sound onset
    EEG.event(event_num + i).duration = 0; 
    EEG.event(event_num + i).trigger  = 1;   % 1 for stim trial,0 for catch, 2 for ECG
end

EEG = eeg_checkset(EEG, 'eventconsistency');   % after add the event, need to check the trigger by order

%% check the catch trial
cat_num         =   1;
epoch_num       =   0;
catch_trial     =   [];
catch_position  =   [];
for i= 1:length(EEG.event)-1
    if strcmp(EEG.event(i).type,'100')
        epoch_num = epoch_num +1;    
        if ~strcmp(EEG.event(i+1).type,'20')  
            catch_position(cat_num) =   EEG.event(i).latency;  % latency for catch trial trigger
            catch_trial(cat_num)    =   epoch_num;             % epoch number of catch trial
            cat_num                 =   cat_num +1;
        end
    end       
end

event_num2 = length(EEG.event);
% add marker 20 for catch trial, just one point after 100(trigger)
for i = 1:length(catch_position)
    EEG.event(event_num2 + i).latency   = catch_position(i)+1;
    EEG.event(event_num2 + i).type      = '20';
    EEG.event(event_num2 + i).duration  = 0;     
    EEG.event(event_num2 + i).trigger   = 0;  % 0 for catch trial, 1 for stim trial, 2 for ECG
end
EEG = eeg_checkset(EEG, 'eventconsistency');    

%%

EEG.sound_start     = sound_start;   % position of sound onset/latency for each sound stim
EEG.catch_trial_eeg = catch_trial;   % epoch number of cathch trial

EEG.stim_trial      = stimonset;     % position of trigger onset/latency for each trigger
EEG.stim_trial(catch_trial) =[];     % exclude catch trials

EEG.stim_delay          = (EEG.sound_start - EEG.stim_trial + 1)/EEG.srate;  % delay between sound stim and trigger stim, second
EEG.stim_delay_time_avg = mean(EEG.stim_delay);     %second
EEG.stim_delay_time_std = std(EEG.stim_delay);      %second

%% read behav data to check if the catch trial from eeg is the same as from behavi
behfiles = dir([cfg.dir_behavior 'main*.mat']);

allbeh=[];
for b = 1:length(behfiles)
    tmp     = load([behfiles(b).folder filesep behfiles(b).name]);
    allbeh  = [allbeh tmp.P.T];
end

%mark the position of catch trial to check with the value from eeg data
EEG.catch_trial_behav = find([allbeh.stimvec] ==0);   % the epoch number/position of catch trial from Behav data
EEG.stim_trial_behav  = find([allbeh.stimvec] ==1);   % the epoch number of stim triam from Behav data

if ~isequal(EEG.catch_trial_eeg,EEG.catch_trial_behav)
    w = sprintf('\ncatch trial number is not correct(behavior catch trial data is not same as detected in EEG)');
    warning(w);
    find((EEG.catch_trial_eeg-EEG.catch_trial_behav) ~=0)
end
%eeglab redraw

%% delay between stim trigger and sound trigger for 500 epoches, for catch trial is 0
EEG.stim_sound_delay = zeros(1,500);
EEG.stim_sound_delay(1,EEG.stim_trial_behav) = EEG.stim_delay;  %

end
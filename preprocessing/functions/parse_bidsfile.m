function [ subj,sess,task,run ] = parse_bidsfile( file )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
idx = strfind(file,'_');
idx2 = strfind(file,'.');

if length(idx) < 4
    idx(4) = Inf;
end
subj = file(1:(idx(1)-1));
sess = file((idx(1)+1):(idx(2)-1));
task = file((idx(2)+1):(idx(3)-1));
run = file((idx(3)+1):min(idx2,idx(4))-1);

end


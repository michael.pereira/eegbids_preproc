function [badsensorsIndex,badepochIndex] = reject_chan(cfg,EEG,Index_veog,Index_heog)

close all
info.metric    = cfg.rejectionmetric;
info.threshold = cfg.rejectionthreshold;
info.methodthreshold = cfg.methodthreshold;
info.veog      = Index_veog; % exclude eogs
info.heog      = Index_heog; % exclude eogs

EEG_temp = EEG;
EEG_temp = pop_select(EEG_temp,'nochannel',[info.veog info.heog]);

info.nchan     = EEG_temp.nbchan;
info.ntrl      = EEG_temp.trials;

level = [];
badsensorsIndex = [];
EEG_temo = [];


if strcmp(info.metric,'zvalue') || strcmp(info.metric, 'maxzvalue')
    runsum = zeros(info.nchan,1);
    runss  = zeros(info.nchan,1);
    runnum = 0;
    for i=1:info.ntrl
        dat = squeeze(EEG_temp.data(:,:,i));
        runsum=runsum+sum(dat,2);
        runss=runss+sum(dat.^2,2);
        runnum=runnum+size(dat,2);
    end
    mval=runsum/runnum;
    sd=sqrt(runss/runnum - (runsum./runnum).^2);
end
for i=1:info.ntrl
    dat = squeeze(EEG_temp.data(:,:,i));
  switch info.metric
    case 'var'
      level(:,i) = std(dat, [], 2).^2;
    case 'min'
      level(:,i) = min(dat, [], 2);
    case 'max'
      level(:,i) = max(dat, [], 2);
    case 'maxabs'
      level(:,i) = max(abs(dat), [], 2);
    case 'range'
      level(:,i) = max(dat, [], 2) - min(dat, [], 2);
    case 'kurtosis'
      level(:,i) = kurtosis(dat, [], 2);
    case '1/var'
      level(:,i) = 1./(std(dat, [], 2).^2);
    case 'zvalue'
      level(:,i) = mean( ( dat-repmat(mval,1,size(dat,2)) )./repmat(sd,1,size(dat,2)) ,2);
    case 'maxzvalue'
      level(:,i) = max( ( dat-repmat(mval,1,size(dat,2)) )./repmat(sd,1,size(dat,2)) , [], 2);
    otherwise
      error('unsupported method');
  end
end

% plot values trials (x-axes) by chan (y-axes):
figure; 
subplot(1,3,1);
imagesc(level);hold on;
xlabel('Trials');
ylabel('Electrodes');

% reject electrodes:
for s=1:info.nchan
    sensor(s,:) = max(level(s,:));
end

switch info.methodthreshold
    case 'median'
        thresholdsensor = (median(sensor)+(cfg.rejectionthreshold*iqr(sensor)));
    case 'mean'
       thresholdsensor = (mean(sensor)+(cfg.rejectionthreshold*std(sensor)));        
end
badsensorsIndex = find(sensor>thresholdsensor);


subplot(1,3,2);
title('Electrodes');hold on;
scatter(sensor,[1:info.nchan],'fill');
line([thresholdsensor thresholdsensor], get(gca, 'ylim'),'color','red');
set(gca,'Ydir','reverse');
ylabel('Electrodes');
xlabel(cfg.rejectionmetric);

% reject epochs:
for ep=1:info.ntrl
    epoch(:,ep) = max(level(:,ep));
end

switch info.methodthreshold
    case 'median'
        thresholdepoch = (median(epoch)+(cfg.rejectionthreshold*iqr(epoch)));
    case 'mean'
       thresholdepoch  = (mean(epoch)+(cfg.rejectionthreshold*std(epoch)));        
end
badepochIndex = find(epoch>thresholdepoch);

subplot(1,3,3);
title('Epochs');hold on;
scatter(epoch,[1:info.ntrl],'fill');
line([thresholdepoch thresholdepoch], get(gca, 'ylim'),'color','red');
set(gca,'Ydir','reverse');
ylabel('Trials');
xlabel(cfg.rejectionmetric);


end
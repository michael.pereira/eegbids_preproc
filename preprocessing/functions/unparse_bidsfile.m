function file = unparse_bidsfile( subj,sess,task,run )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
if nargin == 4
    file = [subj '_' sess '_' task '_' run];
elseif nargin == 3
    file = [subj '_' sess '_' task];
elseif nargin == 2
    file = [subj '_' sess];
elseif nargin == 1
    file = [subj];
end

end


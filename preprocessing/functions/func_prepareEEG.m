function EEG = func_prepareEEG(EEG,cfg)
% EEGorgi1 = EEG;
% Convert data to double precision, recommended for filtering and other
% precedures.
EEG.data = double(EEG.data);

% change to appropriate names of channels
if ~isempty(cfg.heog_chans)
    EEG.chanlocs(cfg.heog_chans(1)).labels = 'HEOG';
end

if ~isempty(cfg.veog_chans)
    EEG.chanlocs(cfg.veog_chans(1)).labels = 'VEOG';
end

EEG.chanlocs(32).labels = 'CPz';

% Add coordintes file:
[EEG] = pop_chanedit(EEG,'lookup','/home/nfaivre/matlab/eeglab14_1_1b/plugins/dipfit2.3/standard_BESA/standard-10-5-cap385.elp');
EEG.backupCPz = EEG.chanlocs(32);

EEG.chanlocs(65).labels = 'Sound';
EEG.chanlocs(68).labels = 'ECG';

% --------------------------------------------------------------
% Downsample data. Removing and adding back the path is necessary for
% avoiding an error of the resample function. Not sure why. Solution is
% explained here: https://sccn.ucsd.edu/bugzilla/show_bug.cgi?id=1184
% --------------------------------------------------------------
if cfg.do_resampling
    [pathstr, ~, ~] = fileparts(which('resample.m'));
    rmpath([pathstr '/'])
    addpath([pathstr '/'])
    [EEG, com] = pop_resample( EEG, cfg.new_sampling_rate);
    EEG = eegh(com, EEG);
end
%eeglab redraw

% --------------------------------------------------------------
% Filter the data.
% --------------------------------------------------------------
% EEGori = EEG;
if cfg.do_hp_filter
    switch(cfg.hp_filter_type)
        case('butterworth') % This is a function of the separate ERPlab toolbox.
            [EEG, com] = pop_ERPLAB_butter1(EEG,cfg.hp_filter_limit,0,5); % requires ERPLAB plugin
            EEG = eegh(com, EEG);
            
        case('kaiser')
            m = pop_firwsord('kaiser', EEG.srate, cfg.hp_filter_tbandwidth, cfg.hp_filter_pbripple);
            beta = pop_kaiserbeta(cfg.hp_filter_pbripple);
            
            [EEG, com] = pop_firws(EEG, 'fcutoff', cfg.hp_filter_limit, ...
                'ftype', 'highpass', 'wtype', 'kaiser', ...
                'warg', beta, 'forder', m);
            EEG = eegh(com, EEG);
            
        case('hamming')
%             m = pop_firwsord('hamming',EEG.srate,cfg.hp_filter_tbandwidth);
            [EEG com b] = pop_eegfiltnew(EEG,cfg.hp_filter_limit);
            EEG      = eegh(com,EEG);
    end
end

if cfg.do_lp_filter
    switch(cfg.lp_filter_type)
        case('blackman')
            [m, ~] = pop_firwsord('blackman', EEG.srate, cfg.lp_filter_tbandwidth);
            [EEG, com] = pop_firws(EEG, 'fcutoff',cfg.lp_filter_limit, 'ftype', 'lowpass', 'wtype', 'blackman', 'forder', m);
            EEG = eegh(com, EEG);
        case('hamming')
%             m = pop_firwsord('hamming', EEG.srate,cfg.lp_filter_tbandwidth);
%             [EEG com b] = pop_eegfiltnew(EEGori,cfg.hp_filter_limit);

            [EEG com b] = pop_eegfiltnew(EEG,[],cfg.lp_filter_limit);
            EEG       = eegh(com,EEG);
    end
    
end


if cfg.do_notch_filter
    m = pop_firwsord('hamming', EEG.srate, 0.2);
    EEG = pop_eegfiltnew(EEG, cfg.notch_filter_lower,...
        cfg.notch_filter_upper,cfg.notch_filter_order,1,[], 1);
end


% ----------------
% Epoch the data.
% ----------------

[EEG, ~, com] = pop_epoch(EEG,{cfg.trig_target},[cfg.epoch_tmin cfg.epoch_tmax], ...
    'newname', 'file epochs', 'epochinfo', 'yes');

% add number and trigger delay(between stim and sound) to each epoch
for i = 1:length(EEG.epoch)
    EEG.epoch(i).epochNum       = i;   % then we can know which epoches are rejected 
    EEG.epoch(i).triggerDelay   = EEG.stim_sound_delay(i);
    EEG.epoch(i).RPeak          = EEG.epoch_RPeak{i};
    EEG.epoch(i).RPeak_min      = EEG.epoch_RPeak_min{i};
    EEG.epoch(i).ecg_RR         = mean(EEG.epoch_ecg_RR{i});
end

EEG = eegh(com, EEG);

% 
%if cfg.do_recenter
    %EEG = pop_rmbase(EEG,[cfg.epoch_tmin cfg.epoch_tmax]);
%    EEG = pop_rmbase(EEG, [],[1:length(EEG.times)]); 
    %EEG = pop_rmbase(EEG, [],[]); 
%end

% Convert back to single precision.
EEG.data = single(EEG.data);



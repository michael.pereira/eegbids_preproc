function [ EEG ] = preproc_lpfilter( EEG, cfg )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
if cfg.lp_filter
        switch(cfg.lp_filter_type)
            case('blackman')
                [m, ~] = pop_firwsord('blackman', EEG.srate, cfg.lp_filter_tbandwidth);
                [EEG, com,b] = pop_firws(EEG, 'fcutoff',cfg.lp_filter_limit, 'ftype', 'lowpass', 'wtype', 'blackman', 'forder', m);
                EEG = eegh(com, EEG);
            case('hamming')
                m = pop_firwsord('hamming', EEG.srate,cfg.lp_filter_tbandwidth);
                %             [EEG com b] = pop_eegfiltnew(EEGori,cfg.hp_filter_limit);
                [EEG com b] = pop_eegfiltnew(EEG,[],cfg.lp_filter_limit);%,[],m,0,0,1);
                EEG       = eegh(com,EEG);
        end
        EEG.lp_filter_coeffs = b;
    end


end


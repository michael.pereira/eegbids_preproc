function EEG = func_QRS(EEG,cfg)
%% need to consider two threshold: 1. threshold for sound; 2. threshold for ECG
%% if has errors from findpeaks, check threshold
EEG0 =[];
EEG0 = EEG;
[EEG0, ~, com] = pop_epoch(EEG0,{cfg.trig_target},[cfg.epoch_tmin cfg.epoch_tmax], ...
    'newname', 'file epochs', 'epochinfo', 'yes');

figure(2);hold on;plot(EEG0.data(68,:,1))
sign = -1; % for ECG peak valuz is pos or negs

sound_peak_th = 0.2E4;     % threshold for sound detection, same value from func_soundOnset

th_QRS_peak   = 700;             % ECG threshold,500-1000, 700 usual
th_QRS_Sample = 500;              % 500, threshold for sample points

for e = 1:length(EEG0.epoch)
    %% find the onset of sound for each epoch, no sound for catch trial
    EEG_sound = [];
    EEG_sound =  EEG0.data(65,:,e) - mean(EEG0.data(65,:,e));       %baseline corr for sound
    
    if ~ismember(e,EEG0.catch_trial_eeg)
        
        sound_peak      = [];
        sound_position  = [];
        
        
        %figure(2); plot(EEG_sound);
        [sound_peak, sound_position] = findpeaks(EEG_sound,'MinPeakHeight',sound_peak_th);
        
        sound_onset = [];   % sample points for the start of each sound stim
        sound_peakV =[];
        
        j = 2;
        for i = 1:length(sound_position)
            if i == 1
                sound_onset(1)  = sound_position (i);
                sound_peakV(1)  = sound_peak(i);
            elseif (sound_position(i) - sound_position(i-1)) > 100
                sound_onset(j)  = sound_position(i);
                sound_peakV(j)  = sound_peak(i);
                j = j + 1;
            end
        end
        
    else  % for catch trial, there is no sound
        sound_onset = 0;
        sound_peakV = 0;
    end
    
    EEG0.data(65,:,e) = EEG_sound;
    %% find onset of R-peak
    
    data_ECG   = [];
    locs_Rwave = [];
    
    data_ECG   = sign*EEG0.data(68,:,e);
    noisyECG_withTrend = data_ECG;
    
    %figure(2);hold on;plot(noisyECG_withTrend)
    %
    
    [p,s,mu] = polyfit((1:numel(noisyECG_withTrend)),noisyECG_withTrend,6);
    f_y = polyval(p,(1:numel(noisyECG_withTrend))',[],mu);
    ECG_data = noisyECG_withTrend - f_y';                   % Detrend data
    
    %figure(3);plot(ECG_data)
    
    %
    [~,locs_Rwave] = findpeaks(ECG_data,'MinPeakHeight',1*th_QRS_peak,'MinPeakDistance',th_QRS_Sample);
    
    %     figure(4)
    %     hold on
    %     plot(ECG_data)
    %     plot(locs_Rwave,ECG_data(locs_Rwave),'rv','MarkerFaceColor','r')
    %
    EEG0.data(68,:,e) = ECG_data;
    
    %% add delay between sound trigger and R peak to epoches
    QRSDelay_all             = (locs_Rwave - sound_onset + 1)/EEG0.srate;      % second
    EEG0.epoch(e).RPeak      = QRSDelay_all;                              % delays for all R peak from sound onset
    if ~ismember(e,EEG0.catch_trial_eeg)
        [QRS_minV,QRS_minP]      = min(abs(QRSDelay_all));
        EEG0.epoch(e).RPeak_min  = QRSDelay_all(QRS_minP);                    % the nearest R peak from sound onset
    else
        EEG0.epoch(e).RPeak_min  = 0;    %for catch trial, no sound, set as 0
    end
    
    if length(QRSDelay_all) == 4      % make sure each epoch has 3 QRS, and get the RR value
        [QRS_maxV,QRS_maxP]  = max(abs(QRSDelay_all));
        locs_Rwave(QRS_maxP) = [];
    end
    
    if length(locs_Rwave) == 3
        ECG_RR = [(locs_Rwave(2) - locs_Rwave(1) +1)/EEG0.srate, (locs_Rwave(3) - locs_Rwave(2) +1)/EEG0.srate];
    elseif length(locs_Rwave) == 2
        ECG_RR = [(locs_Rwave(2) - locs_Rwave(1) +1)/EEG0.srate];
    end
    
    
    EEG0.epoch(e).ecg_RR = ECG_RR;
    
    
    %%
end

EEG.Sound   = EEG0.data(65,:,:);  %signal before downsample
EEG.ECG     = EEG0.data(68,:,:);

EEG.epoch_RPeak     = {EEG0.epoch.RPeak};
EEG.epoch_RPeak_min = {EEG0.epoch.RPeak_min};
EEG.epoch_ecg_RR    = {EEG0.epoch.ecg_RR};

%figure(3);plot(ECG_data)









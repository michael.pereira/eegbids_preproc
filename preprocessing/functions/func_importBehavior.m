function EEG = func_importBehavior(EEG,cfg)

% Load the logfile.
behfiles = dir([cfg.dir_behavior 'main*.mat']);

allbeh=[];
for b = 1:length(behfiles)
    tmp     = load([behfiles(b).folder filesep behfiles(b).name]);
    allbeh  = [allbeh tmp.P.T];
end


% % only for pilot3, as for EEG data, it loss the last sitm at block 4
% block_p3        =  find([allbeh.block] == 4);
% trial_p3        =  find([allbeh.trial] == 50);
% delete_trial    = intersect(block_p3,trial_p3);
% allbeh(delete_trial) = [];


for e = 1:EEG.trials
    EEG.epoch(e).trial          = allbeh(e).trial;
    EEG.epoch(e).block          = allbeh(e).block;
    EEG.epoch(e).stimamp        = allbeh(e).stimamp;
    EEG.epoch(e).stimonset      = allbeh(e).stimonset;
    EEG.epoch(e).stimvec        = allbeh(e).stimvec;
    EEG.epoch(e).startSound     = allbeh(e).startSound;
    EEG.epoch(e).key1           = allbeh(e).key1;
    EEG.epoch(e).rt1            = allbeh(e).rt1;
    EEG.epoch(e).sdt            = allbeh(e).sdt;
    EEG.epoch(e).confidence     = allbeh(e).confidence;
    EEG.epoch(e).rt_conf        = allbeh(e).rt_conf;
end
    
% Issue a warning if the number of trials in the Logfile does not match the
% number in the EEG file.
if length(EEG.epoch) ~= length(allbeh)
    w = sprintf('\nEEG file has %d trials, but Logfile has %d trials.\nYou should check this!', ...
       length(EEG.epoch), length(allbeh));
   warning(w)
end


% Update the EEG.epoch structure.
EEG = eeg_checkset(EEG, 'eventconsistency');


function [ EEG ] = preproc_hpfilter( EEG,cfg )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
%% Filtering
    if cfg.hp_filter
        switch(cfg.hp_filter_type)
            case('butterworth') % This is a function of the separate ERPlab toolbox.
                [EEG, com,b] = pop_ERPLAB_butter1(EEG,cfg.hp_filter_limit,0,5); % requires ERPLAB plugin
                EEG = eegh(com, EEG);
                
            case('kaiser')
                m = pop_firwsord('kaiser', EEG.srate, cfg.hp_filter_tbandwidth, cfg.hp_filter_pbripple);
                beta = pop_kaiserbeta(cfg.hp_filter_pbripple);
                
                [EEG, com,b] = pop_firws(EEG, 'fcutoff', cfg.hp_filter_limit, ...
                    'ftype', 'highpass', 'wtype', 'kaiser', ...
                    'warg', beta, 'forder', m);
                EEG = eegh(com, EEG);
                
            case('hamming')
                [m,dev] = pop_firwsord('hamming',EEG.srate,cfg.hp_filter_tbandwidth);
                [EEG com b] = pop_eegfiltnew(EEG,cfg.hp_filter_limit,[],m,0,0,1);
                EEG      = eegh(com,EEG);
                
        end
        EEG.hp_filter_coeffs = b;
    end
    
    
end


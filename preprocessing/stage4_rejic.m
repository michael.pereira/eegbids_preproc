function m04_rejectICs(selsubj)

   cfg = get_cfg([]);
addpath('functions');
addpath(cfg.dir_eeglab)
eeglab rebuild;
addpath(cfg.dir_jsonlab)

who_idx = dir([cfg.dir_bids '/derivatives/eegprep/sub*']);
if nargin == 0
    selsubj = 1:length(who_idx);
end
for isub = selsubj
        % -------------
        % Prepare data.
        % -------------
        EEG = [];
        cfg = get_cfg(who_idx(isub).name); 
         % Load data set.
        [EEG2 com]= pop_loadset('filename', [cfg.subject_name '_ses-01_stage1.set'],'filepath', cfg.dir_eeg, 'loadmode', 'all');
%         idveog = strcmp({EEG2.chanlocs.labels},'VEOG');
%         idheog = strcmp({EEG2.chanlocs.labels},'HEOG');
%         idecg = strcmp({EEG2.chanlocs.labels},'ECG');
        [EEG2, ~, com] = pop_epoch(EEG2,cfg.trig_target,[cfg.epoch_tmin cfg.epoch_tmax], ...
        'epochinfo', 'yes');
        badtrials = dlmread([cfg.dir_eeg '/' cfg.subject_name '_ses-01_stage2_rejtrials.csv']);
        EEG2.data(:,:,badtrials) = [];
                
         [EEG_1hz com]= pop_loadset('filename', [cfg.subject_name '_ses-01_stage3_1hz.set'],'filepath', cfg.dir_eeg, 'loadmode', 'all');
%         EEG_1hz.data(end+1,:,:) = EEG2.data(idveog,:,:);
%         EEG_1hz.chanlocs(end+1).labels = 'VEOG';
%         EEG_1hz.data(end+1,:,:) = EEG2.data(idheog,:,:);
%         EEG_1hz.chanlocs(end+1).labels = 'HEOG';
%         EEG_1hz.data(end+1,:,:) = EEG2.data(idecg,:,:);
%         EEG_1hz.chanlocs(end+1).labels = 'ECG';
%         EEG_1hz = eeg_checkset(EEG_1hz);
%         
        %% Run SASICA
        EEG_1hz_rmthr = pop_selectevent(EEG_1hz,'type',cfg.trig_target);
        [EEG_1hz_rmthr, com] = SASICA(EEG_1hz_rmthr);     %to use automatic values for thresholds
      % pop_autorejICA()   %         pop_autorejICA('resetprefs_Callback')  
        keyboard
        %%
        x = evalin('base','EEG');
        EEG_1hz.rm_ICs = find(x.reject.gcompreject);
        EEG_1hz = eegh(com,EEG_1hz);
        
        ncomps = sum(x.reject.gcompreject);
        fprintf(['\n I will remove ' num2str(ncomps) ' components\n']);
        
        [EEG_1hz,com] = pop_subcomp(EEG_1hz,EEG_1hz.rm_ICs,1);
        
        if isempty(com)
            return
        end
        EEG_1hz = eegh(com,EEG_1hz);
        %%
        % ----------
        % Save data.
        % ----------
        EEG_1hz = pop_editset(EEG_1hz,'setname',[cfg.subject_name '_ses-01_stage4_1hz.set']);
        EEG_1hz = pop_saveset(EEG_1hz, [cfg.subject_name '_ses-01_stage4_1hz.set'],cfg.dir_eeg);
        
        clear ncomps

    end
fprintf('Done.\n')   

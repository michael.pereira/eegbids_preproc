function [cfg] = get_cfg(EEG_name)
% function [cfg] = get_cfg()
%% Read info for this subject and get file names and dirctories.
rootfilename    = which('get_cfg.m');

user = 'server';

switch(user)
    case 'you'
        cfg.dur_bids = '';
        cfg.dir_eeglab = '';
        cfg.dir_jsonlab = '';
        cfg.dir_cc = '';
    case 'server'
        cfg.dir_bids       = '/Scratch/meaperei/BIDS/agency/';
        cfg.dir_eeglab = '/Scratch/ephys/perithr/toolboxes/eeglab14_1_1b';
        cfg.dir_jsonlab = '/Scratch/ephys/perithr/toolboxes/jsonlab/';
        cfg.dir_cc = '/Scratch/ephys/perithr/toolboxes/circstat/';
        
end

cfg.subject_name    = EEG_name;

cfg.dir_raw         = [cfg.dir_bids '/' cfg.subject_name '/ses-01/eeg/'];
cfg.dir_eeg         = [cfg.dir_bids '/derivatives/eegprep/' cfg.subject_name '/ses-01/eeg/'];   %sound onset epoch
cfg.dir_behavior    = [cfg.dir_bids '/' cfg.subject_name '/ses-01/behav/'];

cfg.events = {};

%%


%%
%% Data organization and content.
% Triggers that mark stimulus onset. These events will be used for
% epoching.
cfg.trig_target = {'OVTK_StimulationId_TrialStart'};          %e.g., [21:29 221:229]; 100  for stim trigger,20 for sound trigger
cfg.epoch_tmin  = -1.000;        %e.g., -2.000;
cfg.epoch_tmax  =  2.000;        %e.g., 0.500;

% Recenter data to zero:
cfg.do_recenter = 1;                  % this is done on the whole epoch
% cfg.epoch_tmin  = cfg.epoch_tmin;   %e.g., -2.000;
% cfg.epoch_tmax  = cfg.epoch_tmax;   %e.g., 0.500;


% what to select bad electrodes à la fieldtrip?
cfg.detectbadchanels = 1;


%% Reference
% Do you want to rereference the data at the import step (recommended)?
% Since Biosemi does not record with reference, this improves signal
% quality. This does not need to be the postprocessing refrence you use for
% subsequent analyses.

% Before ICA:
cfg.ImportReference     = 32;
cfg.do_preproc_reref    = 0;
cfg.preproc_reference   = []; % (31=Pz@Biosemi,32=Pz@CustomM43Easycap) AFTER impor
cfg.do_reref_before_ica = 1;
cfg.do_reref_after_ica  = 0;
cfg.postproc_reference  = [];  % empty = average reference

%% Do you want to have a new sampling rate?
cfg.do_resampling     = 1;
cfg.new_sampling_rate = 256;

%% Interpolate missing channels after ICA:
cfg.interpolMissingChan = 1;
% cfg.chanlocsBE4interpol = '/home/sv/Matlabtoolboxes/Analysis_Data_Scripts/Code/chanlocsBE4interpol.mat';

%% Filter parameters
% Do you want to high-pass filter the data?
cfg.hp_filter            = 1;
cfg.hp_filter_type          = 'hamming';  % or 'butterworth' - not recommended or hamming or kaiser
cfg.hp_filter_limit         = 0.2;
cfg.hp_filter_tbandwidth    = 0.2;
%cfg.hp_filter_pbripple      = 0.01;

% Do you want to low-pass filter the data?
cfg.lp_filter            = 1;
cfg.lp_filter_type          = 'hamming';   % or 'blackman'
cfg.lp_filter_limit         = 40;
cfg.lp_filter_tbandwidth    = 5;

% Do you want to use a notch filter? Note that in most cases Cleanline
% should be sufficient.
cfg.do_notch_filter     = 0;
cfg.notch_filter_lower  = 49;
cfg.notch_filter_upper  = 51;
cfg.notch_filter_order  = []; % try this before with the gui, and change this! as it is related to the EEG.srate

cfg.hp_ICA_filter    = 1;
cfg.hp_ICA_filter_type  = 'hamming';
cfg.hp_ICA_filter_limit = 1;

%% Artifact detection:
% Visual inspection of the data
cfg.do_visual_inspection_preICA  = 1;
cfg.do_visual_inspection_postICA = 1;

% parameters for remove  artifact channels & epochs
cfg.detectbadchanels    = 1;
cfg.rejectbadchanels    = 1;
cfg.rejectbadepochs     = 1;
cfg.rejectionmetric     = 'var';        % 'var' etc. check reject_chan function.
cfg.methodthreshold     = 'median';     % mean or median
cfg.rejectionthreshold  = 5;          % value to multiply with iqr (e.g. threshold is median+2*iqr)

cfg.rejectbadchanelsvisual = 0;


% Do you want to reject trials based on amplitude criterion? (automatic and
% manual)
cfg.do_apmli_rej_preICA = 1;
cfg.do_rej_thresh       = 0;
cfg.rej_thresh_pre      = 400;
cfg.rej_thresh_post     = 150; %200
%cfg.rej_thresh_tmin     = cfg.epoch_tmin;
%cfg.rej_thresh_tmax     = cfg.epoch_tmax;

% ...If not interpolating, do you want to ignore those channels in
% automatic artifact detection methods? 1 = use only the other channels.
cfg.ignore_interp_chans = 0;

%% Parameters for ICA.
% amica
cfg.amica        = 0;

% binica
cfg.binica        = 1;
cfg.ica_type     = 'binica';
cfg.ica_extended = 1;

%[numel(cfg.data_chans)-3]; % if ica_ncomps==0, determine data rank from the data (EEGLAB default). Otherwise, use a fixed number of components. Note: subject-specific
% settings will override this parameter.

%% Parameters for SASICA.
% cfg.sasica_heogchan     = num2str(cfg.data_chans+1);
% cfg.sasica_heogchan     = num2str(cfg.data_chans+2);

cfg.sasica_autocorr     = 20;
cfg.sasica_focaltopo    = 'auto';




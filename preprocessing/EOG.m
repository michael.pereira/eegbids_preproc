% 03
for isub = 2:4
    EEG3 = [];
    % get cfgs
    cfg = get_cfg(who_idx(isub).name);   
    
    % Load data set.
    EEG3 = pop_loadset('filename',[cfg.subject_name '_CleanBeforeICA.set'] , ...
        'filepath',cfg.dir_eeg,'loadmode','all');
    
        
    Index_veog3  = find(contains({EEG3.chanlocs.labels},'VEOG'));
    Index_heog3  = find(contains({EEG3.chanlocs.labels},'HEOG'));
        
    %EEG3_veog_preICA  =   EEG3.data(Index_veog3,:,:);
    %EEG3_heog_preICA  =   EEG3.data(Index_heog3,:,:);
    
    EEG3.epochN = [EEG3.epoch.epochNum];
    
%     EEG = pop_editset(EEG, 'setname', [cfg.subject_name '_ICA_binN.set']);
%     EEG = pop_saveset( EEG, [cfg.subject_name '_ICA_binN.set'],cfg.dir_eeg);
%     
    %%
    EEG5 = [];
    
    EEG5 = pop_editset(EEG5, 'setname', [cfg.subject_name '_VisCleanAfterIca.set']);
    EEG5 = pop_loadset('filename',[cfg.subject_name '_VisCleanAfterIca.set'],...
     'filepath',cfg.dir_eeg,'loadmode','all');
    
    Index_veog5 = find(contains({EEG5.chanlocs.labels},'VEOG'));
    Index_heog5  = find(contains({EEG5.chanlocs.labels},'HEOG'));

 
    EEG5.epochN = [EEG5.epoch.epochNum];

    reject = setdiff(EEG3.epochN,EEG5.epochN);
    %Field = [1:length(EEG.epoch)];
    
    reject2 =find(ismember(EEG3.epochN,reject))
    
    EEG3 = pop_rejepoch( EEG3,[reject2],0);
      
   
    
    EEG5.data(Index_veog5, :, :) =  EEG3.data(Index_veog3,:,:);
    EEG5.data(Index_heog5, :, :) =  EEG3.data(Index_heog3,:,:);
    
    
    EEG5 = pop_editset(EEG5, 'setname', [cfg.subject_name '_VisCleanAfterIcaN.set']);
    EEG5 = pop_saveset(EEG5,[cfg.subject_name '_VisCleanAfterIcaN.set'] ,cfg.dir_eeg);
  
end
    



function m02_prepro_cleanbeforeICA(selsubj)

% eeglab
%[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
cfg = get_cfg([]);
addpath('functions');
addpath(cfg.dir_eeglab)
eeglab rebuild;
addpath(cfg.dir_jsonlab)


who_idx = dir([cfg.dir_bids '/derivatives/eegprep/sub*']);
if nargin == 0
    selsubj = 1:length(who_idx);
end
for isub = selsubj
    %%
    % Load CFG file. I know, eval is evil, but this way we allow the user
    % to give the CFG function any arbitrary name, as defined in the EP
    % struct.
    cfg = get_cfg(who_idx(isub).name);
    EEG = [];
    % --------------
    % Load data set.
    % --------------
    sess = 'ses-01';
    file = [cfg.subject_name  '_' sess '_stage1.set'];
    EEG = pop_loadset('filename',file ,'filepath',cfg.dir_eeg);
        
    
    %% Epoching
    [EEG, ~, com] = pop_epoch(EEG,cfg.trig_target,[cfg.epoch_tmin cfg.epoch_tmax], ...
        'epochinfo', 'yes');
    
%     % save and exclude sound and ecg
%     Index_ecg  = contains({EEG.chanlocs.labels},'ECG');
%     Index_veog    = contains({EEG.chanlocs.labels},'VEOG');
%     Index_heog    = contains({EEG.chanlocs.labels},'VEOG');
%     EEG.save_ecg = EEG.data(Index_ecg,:,:);
%     EEG.save_veog   = EEG.data(Index_veog,:,:);
%     EEG.save_heog   = EEG.data(Index_heog,:,:);
    
    EEG = pop_select(EEG,'nochannel',{'AUDIO','ECG','VEOG','HEOG','MISC','M1','M2'});
    EEG.full_chanlocs = EEG.chanlocs;
    %cpz = strcmp({EEG.chanlocs.labels},'CPz');
    %EEG.data(cpz,:,:) = 0;
    
    if cfg.do_visual_inspection_preICA
        
        col = cell(1,length(EEG.chanlocs));
        for ichan=1:length(EEG.chanlocs)
            col{ichan} = [0 0 0];
        end
        
        %         for ichan = badsensorsIndex'
        %             col{ichan} = [1 0 0];
        %         end
        
        global eegrej
        
        mypop_eegplot(EEG,1,1,1,'winlength',8,'color',col,'command','global eegrej, eegrej = TMPREJ');
        
        disp('Interrupting function now. Waiting for you to press')
        disp('"Update marks", and hit "Continue" in Matlab editor menu')
        
         keyboard
        
        % eegplot2trial cannot deal with multi-rejection
        if ~isempty(eegrej)
            trials_to_delete = [];
            badChnXtrl       = [];
            rejTime          = [];
            
            rejTime = eegrej(:,1:2);
            [~,firstOccurences,~] = unique(rejTime,'rows');
            eegrej = eegrej(firstOccurences,:);
            
            [badtrls, badChnXtrl] = eegplot2trial(eegrej,EEG.pnts,length(EEG.epoch));
            trials_to_delete = find(badtrls);
            EEG.trials_to_delete = trials_to_delete;
            clear eegrej;
            
            % -------------------------------------
            %  Execute SELECTIVE interpolation and rejection
            % -------------------------------------
            EEG = pop_selectiveinterp(EEG,badChnXtrl);
            [EEG, com] = pop_rejepoch(EEG, trials_to_delete, 1);
            
            EEG = eegh(com,EEG);
            
            dlmwrite([cfg.dir_eeg '/' cfg.subject_name  '_' sess '_stage2_rejtrials.csv'],trials_to_delete);
            
    %   
        end
        
    end
    
    % do you want to reject more chans after visual inspection?
    %         rejpchan = inputdlg('More channels to be eliminated','Eliminate');
    %         rejchan2 = find(ismember({EEG.chanlocs.labels},cell2mat(rejpchan)));
    [indx,tf] = listdlg('ListString',{EEG.chanlocs.labels});
    if~isempty(indx)
        [EEG,com]  = pop_select(EEG,'nochannel',indx);
        EEG = eegh(com, EEG);
        EEG = eeg_checkset(EEG);
        channels_to_delete = {EEG.chanlocs(indx).labels};
        fid = fopen([cfg.dir_eeg '/' cfg.subject_name  '_' sess '_stage2_rejchannels.csv'],'w');
        for ch=1:length(indx)
            fprintf(fid,'%s\n',EEG.chanlocs(indx(ch)).labels);
        end
        fclose(fid);   
    end
    
   
    %%
    if cfg.do_reref_before_ica
       
        [~,excl] = intersect({EEG.chanlocs.labels},{'AUDIO' 'ECG','VEOG','HEOG','M1','M2','MISC'});
        [EEG, com]                  =   pop_reref(EEG,[],'exclude',excl,'keepref','off');
        EEG                         =   eeg_checkset(EEG);
    end
    
    
    %% Save data
    fprintf(' [ stage 2 ] saving data\n');
    [EEG, com] = pop_editset(EEG, 'setname', [cfg.subject_name '_' sess '_stage2']);
    EEG = eegh(com, EEG);
    pop_saveset( EEG, 'filename',[cfg.subject_name  '_' sess '_stage2.set'] ,'filepath',cfg.dir_eeg);
    %     if cfg.hp_ICA_filter
    %         [EEG_ica, com] = pop_editset(EEG_ica, 'setname', [cfg.subject_name '_' sess '_icafilt']);
    %         EEG_ica = eegh(com, EEG_ica);
    %         pop_saveset( EEG_ica, 'filename',[cfg.subject_name  '_' sess '_icafilt.set'] ,'filepath',cfg.dir_eeg);
    %     end
    
end
fprintf('Done.\n')



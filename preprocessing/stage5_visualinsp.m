function m05_preprp_afterICArmv(selsubj)

   cfg = get_cfg([]);
addpath('functions');
addpath(cfg.dir_eeglab)
eeglab rebuild;
addpath(cfg.dir_jsonlab)


who_idx = dir([cfg.dir_bids '/derivatives/eegprep/sub*']);
if nargin == 0
    selsubj = 1:length(who_idx);
end
for isub = selsubj

    
    EEG = [];
    
    % get cfgs
    cfg = get_cfg(who_idx(isub).name);

    EEG_1hz = pop_loadset('filename',[cfg.subject_name '_ses-01_stage4_1hz.set'],...
        'filepath',cfg.dir_eeg,'loadmode','all');
     
%    % for AFz
%     EEG1 = [];
%     EEG1  = pop_select(EEG,'channel',{'Fpz' 'Fp1' 'Fp2' 'AF3' 'AF4' 'F1' 'F2' 'Fz'});  %
%     EEG.data(EEG.nbchan+1,:,:)  =  mean(EEG1.data); 
%     EEG.chanlocs(EEG.nbchan+1).labels  =   'AFz';       
%     EEG = eeg_checkset(EEG);
    
    if cfg.do_visual_inspection_postICA
       col = cell(1,length(EEG_1hz.chanlocs));
        for ichan=1:length(EEG_1hz.chanlocs)
            col{ichan} = [0 0 0];
        end
          
        global eegrej
        
        mypop_eegplot(EEG_1hz,1,1,1,'winlength',8,'color',col,'command','global eegrej, eegrej = TMPREJ');
        
        disp('Interrupting function now. Waiting for you to press')
        disp('"Update marks", and hit "Continue" in Matlab editor menu')
        
         keyboard
        
        % eegplot2trial cannot deal with multi-rejection
        if ~isempty(eegrej)
            trials_to_delete = [];
            badChnXtrl       = [];
            rejTime          = [];
            
            rejTime = eegrej(:,1:2);
            [~,firstOccurences,~] = unique(rejTime,'rows');
            eegrej = eegrej(firstOccurences,:);
            
            [badtrls, badChnXtrl] = eegplot2trial(eegrej,EEG_1hz.pnts,length(EEG_1hz.epoch));
            trials_to_delete2 = find(badtrls);
            EEG_1hz.trials_to_delete2 = trials_to_delete2;
            clear eegrej;
            
            % -------------------------------------
            %  Execute SELECTIVE interpolation and rejection
            % -------------------------------------
            EEG_1hz = pop_selectiveinterp(EEG_1hz,badChnXtrl);
            [EEG_1hz, com] = pop_rejepoch(EEG_1hz, trials_to_delete2, 1);
            
            EEG_1hz = eegh(com,EEG_1hz);
            
            dlmwrite([cfg.dir_eeg '/' cfg.subject_name  '_ses-01_stage5_rejtrials.csv'],trials_to_delete2);

        end
    end
    
    if cfg.do_reref_after_ica
        [EEG_1hz, com] = pop_reref(EEG_1hz,[],'keepref','on');
        EEG_1hz = eegh(com, EEG_1hz);
    end
    
     % epoch information
     newepoch = [];
    for e = 1:length(EEG_1hz.epoch)
            idlabel = strncmp(EEG_1hz.epoch(e).eventtype,'OVTK_StimulationId_Label',length('OVTK_StimulationId_Label'));
            if sum(idlabel) > 0
                newepoch(e).label = EEG_1hz.epoch(e).eventtype{idlabel};
            else
                newepoch(e).label = 'n/a';
            end
            
            idnumber = strncmp(EEG_1hz.epoch(e).eventtype,'OVTK_StimulationId_Number',length('OVTK_StimulationId_Number'));
            if sum(idnumber) > 0
                newepoch(e).number = EEG_1hz.epoch(e).eventtype{idnumber};
            else
                newepoch(e).number = 'n/a';
            end
    end
    EEG_1hz.epoch = newepoch;
    EEG_1hz = pop_editset(EEG_1hz, 'setname', [cfg.subject_name '_preproc_1hz.set']);
    EEG_1hz = pop_saveset(EEG_1hz,[cfg.subject_name '_preproc_1hz.set'] ,cfg.dir_eeg);
  
    
end

fprintf('Done.\n')